﻿using System.Collections.Generic;

namespace InflationaryEnglish.Models
{
    public class EnglishTextModel
    {
        public string EnglishText { get; set; }

        public string InflatedEnglishText { get; set; }

        // Order matters for this dictionary because of how I am checking for the existing strings.  Longer keys that contain shorter keys must be listed first.  This is not ideal as it is user error prone.
        private Dictionary<string, int> _soundsLikeNumberEnglishMap = new Dictionary<string, int>
        {
            {"won", 1},

            {"too", 2},
            {"to", 2},

            {"tree", 3},

            {"fore", 4},
            {"for", 4},

            //{"?", 5},

            {"sics", 6},

            //{"?", 7},

            {"ate", 8},

            //{"?", 9},

            {"tin", 10},
        };

        private Dictionary<int, string> _numberToStringMap = new Dictionary<int, string>
        {
            {1, "one"},
            {2, "two"},
            {3, "three"},
            {4, "four"},
            {5, "five"},
            {6, "six"},
            {7, "seven"},
            {8, "eight"},
            {9, "nine"},
            {10, "ten"},
            {11, "eleven"},
        };

        // Currently does not handle case insensitive search correctly :/ 
        public void InflateEnglish()
        {
            var modifiedEnglishText = EnglishText;
            foreach (var key in _soundsLikeNumberEnglishMap.Keys)
            {
                if (modifiedEnglishText.Contains(key))
                {
                    // Get the numeric version of the number sounding word and inflate.
                    var index = _soundsLikeNumberEnglishMap[key];
                    index++;
                    modifiedEnglishText = modifiedEnglishText.Replace(key, _numberToStringMap[index]);
                }

                /*                var firstIndex = EnglishText.IndexOf(key, StringComparison.InvariantCultureIgnoreCase) + key.Length;
                                var subString = EnglishText.Substring(firstIndex - key.Length, key.Length);*/
            }
            InflatedEnglishText = modifiedEnglishText;
        }
    }
}