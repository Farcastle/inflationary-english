﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using InflationaryEnglish.Models;

namespace InflationaryEnglish.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var model = new EnglishTextModel
            {
                EnglishText = "Today I won an award for being awesome"
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(EnglishTextModel model)
        {
            model.InflateEnglish();
            return View(model);
        }
    }
}