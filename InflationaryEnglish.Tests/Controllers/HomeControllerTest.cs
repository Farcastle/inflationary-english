﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using InflationaryEnglish.Controllers;
using InflationaryEnglish.Models;

namespace InflationaryEnglish.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            HomeController controller = new HomeController();
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }

        /*
         * This is not working as I didn't have time to implement a solution yet.
         * */
        [TestMethod]
        public void CaseInsentiveTest()
        {
            var model = new EnglishTextModel
            {
                EnglishText = "Today I won an award for being awesome."
            };

            model.InflateEnglish();

            // Today should be converted to Threeday
            Assert.IsFalse(model.InflatedEnglishText.Contains("Today"));
            Assert.IsTrue(model.InflatedEnglishText.Contains("Threeday"));
        }

        [TestMethod]
        public void MultipleReplacementsTest()
        {
            var model = new EnglishTextModel
            {
                EnglishText = "Today I won an award for being awesome"
            };

            model.InflateEnglish();

            // won should be converted to two
            Assert.IsTrue(model.InflatedEnglishText.Contains("two"));

            // for should be converted to five
            Assert.IsTrue(model.InflatedEnglishText.Contains("five"));
        }

        [TestMethod]
        public void OrderOfOperationsReplacementsTest()
        {
            var model = new EnglishTextModel
            {
                EnglishText = "I am too tired to go to the car."
            };

            model.InflateEnglish();

            // too should be converted to two
            Assert.IsTrue(model.InflatedEnglishText.Equals("I am three tired three go three the car."));

            /* if "to" is checked before "too" then you end up with an incorrect replacement such as:
             * 
             * I am threeo tired three go three the car.
             */
        }

        /*
         * This is not working.  A better method of detecting the words that sound like numbers would be needed such as a soundex function.
         * */
        [TestMethod]
        public void CorrectTextButIncorrectSoundingTextTest()
        {
            var model = new EnglishTextModel
            {
                EnglishText = "The store is very far."
            };

            model.InflateEnglish();

            // No replacement should be made as the "to" in "store" does not sound like the number two.
            Assert.IsTrue(model.InflatedEnglishText.Equals(model.EnglishText));
        }
    }
}
